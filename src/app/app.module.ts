import { HomeComponent } from './components/home/home.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatTabsModule} from '@angular/material/tabs';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { RetiroComponent,DialogConfirmarRetiro } from './components/retiro/retiro.component';
import { SaldoComponent, DialogSaldo } from './components/saldo/saldo.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { TransaccionService } from './service/transaccion.service';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DialogConfirmarRetiro,
    RetiroComponent,
    SaldoComponent,
    DialogSaldo
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    FormsModule,
    MatProgressSpinnerModule,
    HttpClientModule

  ],
  entryComponents:[
    DialogConfirmarRetiro,DialogSaldo
  ],
  providers: [TransaccionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
