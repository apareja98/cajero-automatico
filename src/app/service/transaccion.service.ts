import { GLOBAL } from './../global';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TransaccionService {
  url: string = GLOBAL.url + 'transaccionBancaria/';
  constructor(private http: HttpClient) { }

  public retirar(cuenId:string, valor:number, clave:string):Observable<any>{
    let body={cuenId:cuenId,usuUsuario:'ATM',valor:valor, clave:clave};
    return this.http.post(this.url+'retirarATM',body);
  }

  public saldo(cuenId:String, clave:String):Observable<any>{
    return this.http.get(this.url+'consultarSaldoATM/'+ cuenId + '/' + clave);
  }
}
