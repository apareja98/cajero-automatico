import { TransaccionService } from './../../service/transaccion.service';
import { DialogData } from './../retiro/retiro.component';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';

export interface DialogData {
  titulo: string;
  mensaje: string;
}

@Component({
  selector: 'app-saldo',
  templateUrl: './saldo.component.html',
  styleUrls: ['./saldo.component.css']
})
export class SaldoComponent implements OnInit {
  clave : String;
  cuentaId : String;
  hide : boolean =true;
  mostrarLoading= false;
  constructor(public dialog: MatDialog, public ts: TransaccionService) { }

  ngOnInit() {
  }

  consultarSaldo(): void{
    if(!(this.cuentaId && this.clave)){
      this.openDialog("Campos faltantes", "Revise todos los campos y vuelva a intentar");
    }else{
    this.mostrarLoading=true;

      this.ts.saldo(this.cuentaId,this.clave).subscribe(res=>{
        this.openDialog(res.mensaje, "Saldo: $" + res.saldo);
        console.log(res);
        this.mostrarLoading=false;
      },error=>{
        console.log(error.error);
        this.openDialog("Error", error.error.mensaje);
        this.mostrarLoading =false;
      });
     
    }
    this.limpiarCampos();

  }
  limpiarCampos(){
    this.cuentaId=null;
    this.clave=null;

  }

  openDialog(titulo:string, mensaje:string): void {
    const dialogRef = this.dialog.open(DialogSaldo, {
      width: '400px',
      data: {titulo: titulo, mensaje: mensaje}
    });

   
  }

}

@Component({
  selector: 'dialog-saldo',
  templateUrl: 'dialog-consulta-saldo.html',
})
export class DialogSaldo {
 
  constructor(
    public dialogRef: MatDialogRef<DialogSaldo>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      console.log(data);
    }

  onNoClick(): void {
    this.dialogRef.close();
  }
  

}
