import { TransaccionService } from './../../service/transaccion.service';
import { MatDialog } from '@angular/material';
import { Component, OnInit,Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

export interface DialogData {
  titulo: string;
  mensaje: string;
}

@Component({
  selector: 'app-retiro',
  templateUrl: './retiro.component.html',
  styleUrls: ['./retiro.component.css']
})
export class RetiroComponent implements OnInit {

  constructor(public dialog: MatDialog, public ts: TransaccionService) { }
  cuentaId:string;
  clave:string;
  valor:number;
  hide = true;
  mostrarLoading= false;
  ngOnInit() {
  }

  realizarRetiro():void{
    if(!(this.cuentaId && this.clave && this.valor)){
      this.openDialog("Datos incompletos", "Asegurese de ingresar todos los datos");
    }else{
      this.mostrarLoading =true;
      this.ts.retirar(this.cuentaId,this.valor,this.clave).subscribe(res=>{
        this.mostrarLoading=false;
        this.openDialog("Retiro exitoso", "Saldo de la cuenta: " + res.saldo);
        this.limpiarCampos();
      },error=>{
        this.mostrarLoading=false;
        this.openDialog("Retiro fallido", error.error.mensaje);
        this.limpiarCampos();
      })

    }
  }
  limpiarCampos(){
    this.clave="";
    this.cuentaId="";
    this.valor=null;
  }
  openDialog(titulo:string, mensaje:string): void {
    const dialogRef = this.dialog.open(DialogConfirmarRetiro, {
      width: '400px',
      data: {titulo: titulo, mensaje: mensaje}
      
    });

   
  }

}
@Component({
  selector: 'dialog-confirmar-retiro',
  templateUrl: 'dialog-confirmar-retiro.html',
})
export class DialogConfirmarRetiro {
 
  constructor(
    public dialogRef: MatDialogRef<DialogConfirmarRetiro>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      console.log(data);
    }

  onNoClick(): void {
    this.dialogRef.close();
  }
  

}
